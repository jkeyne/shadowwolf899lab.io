Title: Prototype
Date: 03/02/2017 23:08
Category: EPIC

This shows the very first prototype I built on a breadboard and using modified example code. So far this is also my only prototype, and I hope to finish the code as soon as I place the order for my custom PCB.

[First prototype]({filename}/videos/prototype1.mp4)
